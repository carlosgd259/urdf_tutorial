#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>


double deltaPan;
double deltaTilt;
double scale;
const double degree2rad = M_PI/180;

float rotx;
float rotz;

float prevrotz=0;
float prevrotx=0;

void velmessagereceived(const geometry_msgs::Twist& msg) {
    rotx = msg.linear.x;
    rotz = msg.angular.z;
    
    
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  ros::NodeHandle nh;
  //The node advertises the joint values of the pan-tilt

  ros::Subscriber sub = nh.subscribe("/teleop_values", 1000,
  &velmessagereceived);
  
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    
  ros::Rate loop_rate(30);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(2);
  joint_state.position.resize(2);
  double pan = 0.0;
  double tilt = 0.0;

 // deltaPan = 0.0;
 // deltaTilt = 0.0;
 // scale =0.5;


  while (ros::ok())
  {
      //moving one degree
      //deltaPan =  degree2rad * scale;
     // deltaTilt = degree2rad * scale;      
     // pan = pan + deltaPan;
     // tilt = tilt + deltaTilt;

    
       pan = pan + (rotx/2)*degree2rad;
       tilt = tilt + (rotz/2)*degree2rad;


      //ROS_INFO_STREAM("rotx:= "
	//<<pan <<" rotz:= "<<tilt);

      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="pan_joint";
      joint_state.position[0] = pan;
      joint_state.name[1] ="tilt_joint";
      joint_state.position[1] = tilt;

      //send the joint state 
      joint_pub.publish(joint_state);

      loop_rate.sleep();
      ros::spinOnce();
  }
  return 0;
}



