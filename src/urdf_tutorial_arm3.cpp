#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>
#include <urdf_tutorial/changecontrolledjoints.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>


double deltaPan;
double deltaTilt;
double scale=1;
const double degree2rad = M_PI/180;
int joint1=0;
int joint2=1;
float rotx;
float rotz;

float prevrotz=0.0;
float prevrotx=0.0;
bool scalechanged=false;
bool jointchanged=false;
float newscale = 0.0;

void velmessagereceived(const geometry_msgs::Twist& msg) {
    rotx = msg.linear.x;
    rotz = msg.angular.z;
}

bool changeScale(
        urdf_tutorial::changescale::Request &req,
        urdf_tutorial::changescale::Response &resp){

        scale = req.s;
        scalechanged = true;
        return true;
}


bool changeJoint(
        urdf_tutorial::changecontrolledjoints::Request &req,
        urdf_tutorial::changecontrolledjoints::Response &resp){

	if (req.c1>6 || req.c2>6 ||req.c1<0 || req.c2<0){
	ROS_INFO_STREAM("Error, teleoperable joints are from 0 to 6");
        }
        else{
        joint1 = req.c1;
        joint2 = req.c2;
        }
        jointchanged = true;
        return true;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  ros::init(argc, argv, "robot_state_listener");

  
  
  //The node advertises the joint values of the pan-tilt

  ros::Subscriber sub = n.subscribe("/teleop_values", 1000,
  &velmessagereceived);
  
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    
  ros::Rate loop_rate(30);
   
  ros::ServiceServer server0 =
          n.advertiseService("urdf_tutorial/change_scale",&changeScale);
  ros::ServiceServer server1 =
          n.advertiseService("urdf_tutorial/change_joints",&changeJoint);
  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(9);
  joint_state.position.resize(9);
  double pan = 0.0;
  double tilt = 0.0;

 // deltaPan = 0.0;
 // deltaTilt = 0.0;
 // scale =0.5;
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);
  
  while (ros::ok())
  {
   geometry_msgs::TransformStamped transformStamped;

     // ROS_INFO_STREAM("value in z "<<transformStamped2.transform.translation.x);

      pan = pan + (rotx/2)*degree2rad*scale;
      tilt = tilt + (rotz/2)*degree2rad*scale;
      
      // Limits for joint 0, shoulder_pan_joint
      if (joint1 == 0 ){
	  
          if (pan < -150*degree2rad){
	       pan = -150*degree2rad;
	  }
	  else if (pan > 114*degree2rad){
	       pan = 114*degree2rad;
	  }
     }

      if (joint2 == 0 ){
	  
          if (tilt < -150*degree2rad){
	       tilt = -150*degree2rad;
	  }
	  else if (tilt > 114*degree2rad){
	       tilt = 114*degree2rad;
	  }
     }

      // Limits for joint 1, shoulder_pitch_joint

      if (joint1 == 1 ){
          if (pan < -67*degree2rad){
	       pan = -67*degree2rad;
	  }
	  else if (pan > 114*degree2rad){
	       pan = 114*degree2rad;
	  }
     }

      if (joint2 == 1 ){
	  
          if (tilt < -67*degree2rad){
	       tilt = -67*degree2rad;
	  }
	  else if (tilt > 114*degree2rad){
	       tilt = 114*degree2rad;
	  }
     }

      // Limits for joint 2, elbow_roll_joint

      if (joint1 == 2 ){
	  
          if (pan < -150*degree2rad){
	       pan = -150*degree2rad;
	  }
	  else if (pan > 41*degree2rad){
	       pan = 41*degree2rad;
	  }
     }

      if (joint2 == 2 ){
	  
          if (tilt < -150*degree2rad){
	       tilt = -150*degree2rad;
	  }
	  else if (tilt > 41*degree2rad){
	       tilt = 41*degree2rad;
	  }
     }

      // Limits for joint 3, elbow_pitch_joint

      if (joint1 == 3){
	  
          if (pan < -92*degree2rad){
	       pan = -92*degree2rad;
	  }
	  else if (pan > 110*degree2rad){
	       pan = 110*degree2rad;
	  }
     }

      if (joint2 == 3){
	  
          if (tilt < -92*degree2rad){
	       tilt = -92*degree2rad;
	  }
	  else if (tilt > 110*degree2rad){
	       tilt = 110*degree2rad;
	  }
     }
     
      // Limits for joint 4, wrist_roll_joint

      if (joint1 == 4 ){
	  
          if (pan < -150*degree2rad){
	       pan = -150*degree2rad;
	  }
	  else if (pan > 150*degree2rad){
	       pan = 150*degree2rad;
	  }
     }

      if (joint2 == 4 ){
	  
          if (tilt < -150*degree2rad){
	       tilt = -150*degree2rad;
	  }
	  else if (tilt > 150*degree2rad){
	       tilt = 150*degree2rad;
	  }
     }
     

      // Limits for joint 5, wrist_pitch_joint
      if (joint1 == 5 ){
	  
          if (pan < -92*degree2rad){
	       pan = -92*degree2rad;
	  }
	  else if (pan > 113*degree2rad){
	       pan = 113*degree2rad;
	  }
     }

      if (joint2 == 5 ){
	  
          if (tilt < -92*degree2rad){
	       tilt = -92*degree2rad;
	  }
	  else if (tilt > 113*degree2rad){
	       tilt = 113*degree2rad;
	  }
     }
   

      // Limits for joint 6, gripper_roll_joint

      if (joint1 == 6 ){
	  
          if (pan < -150*degree2rad){
	       pan = -150*degree2rad;
	  }
	  else if (pan > 150*degree2rad){
	       pan = 150*degree2rad;
	  }
     }

      if (joint2 == 6 ){
	  
          if (tilt < -150*degree2rad){
	       tilt = -150*degree2rad;
	  }
	  else if (tilt > 150*degree2rad){
	       tilt = 150*degree2rad;
	  }
     }
     



      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="shoulder_pan_joint";
      joint_state.name[1] ="shoulder_pitch_joint";
      joint_state.name[2] ="elbow_roll_joint";
      joint_state.name[3] ="elbow_pitch_joint";
      joint_state.name[4] ="wrist_roll_joint";
      joint_state.name[5] ="wrist_pitch_joint";
      joint_state.name[6] ="gripper_roll_joint";
      joint_state.name[7] ="finger_joint1";
      joint_state.name[8] ="finger_joint2";


      joint_state.position[joint1] = pan;
      joint_state.position[joint2] = tilt;
      joint_pub.publish(joint_state);

      try{
       transformStamped = tfBuffer.lookupTransform("", "grasping_frame",
                              ros::Time(0));
              
      }
      catch (tf2::TransformException &ex) {
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
        continue;
      }
      loop_rate.sleep();
      ros::spinOnce();
  }
  return 0;
}


