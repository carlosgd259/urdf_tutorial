#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/Twist.h>
#include <urdf_tutorial/changescale.h>

double deltaPan;
double deltaTilt;
double scale=1;
const double degree2rad = M_PI/180;

float rotx;
float rotz;

float prevrotz=0.0;
float prevrotx=0.0;
bool scalechanged=false;
float newscale = 0.0;

void velmessagereceived(const geometry_msgs::Twist& msg) {
    rotx = msg.linear.x;
    rotz = msg.angular.z;
    
    
}


bool changeScale(
        urdf_tutorial::changescale::Request &req,
        urdf_tutorial::changescale::Response &resp){

        ROS_INFO_STREAM("Changing scale "<<req.s);

        scale = req.s;
        scalechanged = true;
        return true;
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "urdf_tutorial");
  ros::NodeHandle n;
  
  //The node advertises the joint values of the pan-tilt

  ros::Subscriber sub = n.subscribe("/teleop_values", 1000,
  &velmessagereceived);
  
  ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    
  ros::Rate loop_rate(30);
   
  ros::ServiceServer server0 =
          n.advertiseService("urdf_tutorial/change_scale",&changeScale);

  // message declarations
  sensor_msgs::JointState joint_state;
  joint_state.name.resize(2);
  joint_state.position.resize(2);
  double pan = 0.0;
  double tilt = 0.0;

 // deltaPan = 0.0;
 // deltaTilt = 0.0;
 // scale =0.5;


  while (ros::ok())
  {
      //moving one degree
      //deltaPan =  degree2rad * scale;
     // deltaTilt = degree2rad * scale;      
     // pan = pan + deltaPan;
     // tilt = tilt + deltaTilt;

    
       pan = pan + (rotx/2)*degree2rad*scale;
       tilt = tilt + (rotz/2)*degree2rad*scale;


      //ROS_INFO_STREAM("rotx:= "
	//<<pan <<" rotz:= "<<tilt);

      //update joint_state
      joint_state.header.stamp = ros::Time::now();
      joint_state.name[0] ="pan_joint";
      joint_state.position[0] = pan;
      joint_state.name[1] ="tilt_joint";
      joint_state.position[1] = tilt;

      //send the joint state 
      joint_pub.publish(joint_state);

      loop_rate.sleep();
      ros::spinOnce();
  }
  return 0;
}



